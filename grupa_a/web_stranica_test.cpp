#include <iostream>
#include "web_stranica.hpp"

int main() {
  WebStranica webStranica1;
  webStranica1.info();

  WebStranica webStranica2("www.vub.hr");
  webStranica2.setUcitano(true);
  webStranica2.info();

  if (webStranica1 == webStranica2) {
    std::cout << "webStranica1 i webStranica2 su iste." << std::endl;
  } else {
    std::cout << "webStranica1 i webStranica2 nisu iste." << std::endl;
  }

  WebStranica webStranica3("www.google.com", 443);
  webStranica3.setUcitano(true);
  webStranica3.info();
}