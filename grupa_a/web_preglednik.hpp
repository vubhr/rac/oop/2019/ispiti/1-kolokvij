#include "web_stranica.hpp"

class WebPreglednik {
public:
  WebPreglednik(std::string ime, std::string url);
  WebPreglednik(std::string ime, std::string url, std::string incognitoUrl);
  WebPreglednik(const WebPreglednik &webPreglednik);
  ~WebPreglednik();

  void setOtvorenaStranica(WebStranica webStranica);
  void setIncognitoStranica(WebStranica *webStranica);
  WebStranica& getStranicaRef();

  WebPreglednik& operator=(const WebPreglednik &webPreglednik);

  void info() const;  
private:
  std::string ime;
  WebStranica otvorenaStranica;
  WebStranica *incognitoStranica;
};